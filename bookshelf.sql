-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2017 at 01:10 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookshelf`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `bookId` int(11) NOT NULL,
  `bookTitle` varchar(200) NOT NULL,
  `bookIsbn` varchar(100) NOT NULL,
  `bookAuthour` varchar(200) NOT NULL,
  `bookImage` varchar(200) NOT NULL,
  `bookDesc` text NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`bookId`, `bookTitle`, `bookIsbn`, `bookAuthour`, `bookImage`, `bookDesc`, `createdDate`) VALUES
(1, 'personlaitydev', 'ISBN1487917715', 'shiva khera', '1488015548_index.jpg', 'dummy content', '2017-02-25 10:39:08'),
(2, 'analytics ', 'ISBN1487917860', 'john', '1488015647_index.png', 'asdsadsd dfdf', '2017-02-25 10:52:42'),
(3, 'hadoop', 'ISBN1487937251', 'michel lewick', '1487937251_th-id=OP.jpg', 'hadoop content', '2017-02-24 12:54:32'),
(4, 'testbook', 'ISBN1487943250', 'testauth', '1487943250_th-id=OP.jpg', 'dasdsad', '2017-02-24 14:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `userbookshelf`
--

CREATE TABLE `userbookshelf` (
  `shelfId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `bookId` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `rating` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userbookshelf`
--

INSERT INTO `userbookshelf` (`shelfId`, `userId`, `bookId`, `createdDate`, `rating`) VALUES
(1, 1, 2, '2017-02-24 07:31:00', '5'),
(3, 1, 1, '2017-02-24 12:43:55', '3'),
(4, 1, 3, '2017-02-24 12:54:12', '3'),
(5, 1, 4, '2017-02-24 14:34:10', '3');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `log_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture_url`, `profile_url`, `created`, `modified`, `log_type`) VALUES
(1, '', '', 'sasi', 'priya', 'sasipriya.nk@gmail.com', 'female', '', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Google');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`bookId`);

--
-- Indexes for table `userbookshelf`
--
ALTER TABLE `userbookshelf`
  ADD PRIMARY KEY (`shelfId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `bookId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `userbookshelf`
--
ALTER TABLE `userbookshelf`
  MODIFY `shelfId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

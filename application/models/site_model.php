<?php

  class Site_model extends CI_Model
  {
function __construct()
    {
        // Call the Model constructor
$this->load->database();

        parent::__construct();
		
    }
   
    public function count_rows($tbl, $cond = "")
    {

      $this->db->select('COUNT(*) as num');

      $this->db->from($tbl);

      $this->db->where($cond);

      $query = $this->db->get();

      $row = $query->row();

      return $row->num;
    }

    public function FetchData($tbl, $sel = '*', $cond = '', $alterBy = '', $orderBy = 'ASC')
    {
      $this->db->select($sel);
      $this->db->from($tbl);
      $this->db->where($cond);
      if ($alterBy != "") {
        $this->db->order_by($alterBy, $orderBy);
      }
      $query = $this->db->get();

      return $query->result_array();
    }


    public function FetchSingle($tbl, $sel, $cond)
    {

      $this->db->select($sel);

      $this->db->from($tbl);

      $this->db->where($cond);

      $query = $this->db->get();

      $row = $query->row();

      if (isset($row->$sel)) {

        $dts = $row->$sel;

      } else {

        $dts = false;

      }

      return $dts;

    }

    public function UpdateData($tbl, $data, $where)
    {


      $str = $this->db->update($tbl, $data, $where);

      return $this->db->affected_rows();

    }

    public function InsertData($tbl, $data)
    {

      $str = $this->db->insert($tbl, $data);

      return $this->db->insert_id();

      //	echo $this->db->last_query();

    }

    public function InsertBatchData($tbl, $data)
    {

      $str = $this->db->insert_batch($tbl, $data);

      // $str = $this->db->insert($tbl, $data);

      return $this->db->insert_id();

    }

    public function DeleteData($tbl, $cond)
    {

      $this->db->where($cond);

      $this->db->delete($tbl);

      return $this->db->affected_rows();

    }

    public function FetchQuery($tbl, $sel = '*', $cond = '')
    {

      if ($cond != "") {

        $query = $this->db->query("SELECT $sel FROM {PRE}$tbl WHERE $cond");

      } else {

        $query = $this->db->query("SELECT $sel FROM {PRE}$tbl");

      }

      return $query->result_array();

    }

   

    public function validateForm($arr, $arr1 = array())
    {
      $this->load->library('form_validation');
      if (!empty($arr)) {
        foreach ($arr as $ky => $ar) {
          $ky1 = $ky;
          //print_r($arr1);
          if (array_key_exists($ky, $arr1)) {
            $ky1 = $arr1[$ky];
          }
          $this->form_validation->set_rules($ky, $ky1, $ar);
        }
      }

      if ($this->form_validation->run() == FALSE) {

        $errors = array();
        foreach ($arr as $kk => $field) {
          if (form_error($kk)) {

            $errors[$kk] = form_error($kk, '&nbsp;', '&nbsp;');
          }
        }

        return $errors;
      } else {
        return true;
      }
    }

    

    public function get_user_id($un = "")
    {
       $arr = array("email" => $un);
      $uid = $this->FetchSingle('users', 'id', $arr);
      if (isset($uid) && is_numeric($uid)) {
        return $uid;
      } else {
        return -1;
      }
    }

    
   
    public function get_user_type($uid = "")
    {

      $utyp = $this->native_session->userdata('user_type');
      if (($uid == "") && !empty($utyp)) {
        return $utyp;
      } else {
        $uid = $this->get_user_id($uid);

        return $utype = $this->FetchSingle('users', 'log_type', array('id' => $uid));
      }

    }

    public function join_two_tables($sel, $table1, $table2, $id1, $id2, $cond)
    {

      $this->db->select($sel);
      $this->db->from($table1);
      $this->db->join($table2, "$table1.$id1 = $table2.$id2", 'right');
      $this->db->where($cond);
      $query = $this->db->get();

      return $query->result_array();

    }

    public function inner_join($table1, $id1, $id2, $table2, $sel, $cond)
    {

      $this->db->select($sel);
      $this->db->from($table1);
      $this->db->where($cond);
      $this->db->join($table2, "$table1.$id1 = $table2.$id2");
      $query = $this->db->get();

      return $query->result_array();

    }

    
    

  }
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   class Login_model extends Site_model
    {
		//Fetch user shelf book data
     public function fetchBookdata($userid) {

        $this->db->select('books.*,userbookshelf.*');
        $this->db->from('userbookshelf');
        $this->db->join('books', "userbookshelf.bookId = books.bookId", 'left');
        $this->db->where('userbookshelf.userId', $userid);
        $query = $this->db->get();
        return $query->result();
    }
	//Fetchbooks list
	public function fetchBooklist($search) {

        $this->db->select('books.*');
        $this->db->from('books');
        $this->db->where('books.bookTitle LIKE "'.$search.'%"');
    	$this->db->or_where('books.bookIsbn LIKE "'.$search.'%"');
        $query = $this->db->get();
        return $query->result();
    }
	public function fetchuserlist($search) {

        $this->db->select('users.*');
        $this->db->from('users');
       $this->db->where('users.first_name LIKE "'.$search.'%"');
    	$query = $this->db->get();
        return $query->result();
    }
	public function FetchBookCount($tbl,$sel,$cond)

	{

		$this->db->select('count(*) as num');

		$this->db->from($tbl);

		$this->db->where($cond);

		$query = $this->db->get();

		 $row=$query->row();
return $row->num;
		//return $row->num;
		

	}
public function fetchrecord($id){
         $this->db->select('books.*');
        $this->db->from('books');
       $this->db->where("books.bookId",$id);
        $query = $this->db->get();
        return $query->row();
     }
    public function FetchBookeditCount($tbl,$sel,$cond,$id)

	{

		$this->db->select('*');

		$this->db->from($tbl);

		$this->db->where($cond);
                  $where= "(bookId!='".$id."')";
		$query = $this->db->get();

		$row=$query->row();

		return $row;
		

	}
}

<?php
abstract class Site_controller extends CI_Controller
{
 
	public function __construct()
	{

		parent::__construct();
		
    

	}
	public function view_page($page,$data=array())
	{
		if(!isset($data["page_title"]))
		{
			$segm1 = ucfirst(str_replace("_"," ",$this->uri->segment(1)));
			$segm2 = ucfirst(str_replace("_"," ",$this->uri->segment(2)));
			if($segm2!="")
			$segm1 = $segm2.' | '.$segm1;
            $data["page_title"]=$segm1.' | Onlinebook shelf';
		}
    
		if(!isset($data["message"]))
		{
			$data["message"]="";
		}

		  
	 $this->load->view('header', $data);
   
    $this->load->view($page, $data);
	
    
	}
	
    
   
}

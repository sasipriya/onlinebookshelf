<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
*	Version 1.0 - 14/01/2012 - jeroen.moors@fluoline.net  
*
*	Oauthclient a oAuth client library to access 
* 		- Twitter  (oAuth v1)
* 		- Linkedin (oAuth v1)
* 		- Facebook (oAuth v2)
*
*	More info: http://jeroen.is/oauthclient
*/


class Oauthclient {
	var $consumerKey;
	var $consumerSecret;
	
	var $oauthToken;
	var $oauthTokenSecret;
	
	var $serviceType;
	
	var $oauthDataStore;

	var $responseUrl;
	
	var $userId;
	    
    var $_oAuthVersion;
    var $_oAuthHost;
    var $_oAuthAuthorizeUrl;
    var $_oAuthAccessTokenUrl;
    var $_oAuthRedirectUrl;
    var $_callMyProfile;
    
	function __construct()
	{
        $this->ci =& get_instance();
    }
    	
	function setService($service) {
		switch($service) {
			case "twitter":
				$this->_oAuthVersion 			= 1;
				$this->_oAuthRequestTokenUrl 	= "https://api.twitter.com/oauth/request_token";
				$this->_oAuthAuthorizeUrl		= "https://api.twitter.com/oauth/authorize";
				$this->_oAuthAccessTokenUrl		= "https://api.twitter.com/oauth/access_token";
				$this->_oAuthRedirectUrl		= "https://twitter.com/oauth/authenticate?oauth_token=";

				$this->_callMyProfile			= "http://twitter.com/account/verify_credentials.xml";
				break;
				
			case "linkedin":
				$this->_oAuthVersion 	= 1;

				$this->_oAuthRequestTokenUrl 	= "https://api.linkedin.com/uas/oauth/requestToken";
				$this->_oAuthAuthorizeUrl		= "https://api.linkedin.com/uas/oauth/authorize";
				$this->_oAuthAccessTokenUrl		= "https://api.linkedin.com/uas/oauth/accessToken";
				$this->_oAuthRedirectUrl		= "https://www.linkedin.com/uas/oauth/authorize?oauth_token=";

				$this->_callMyProfile			= "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url,twitter-accounts,industry)";
				break;

			case "facebook":
				$this->_oAuthVersion 			= 2;
				$this->_oAuthRedirectUrl		= "https://www.facebook.com/dialog/oauth?scope=offline_access&client_id=".$this->consumerKey."&redirect_uri=".urlencode($this->responseUrl)."&scope=email,user_location,user_work_history,user_education_history";
				$this->_oAuthAccessTokenUrl 	= "https://graph.facebook.com/oauth/access_token?client_id=".$this->consumerKey."&redirect_uri=".urlencode($this->responseUrl)."&client_secret=".$this->consumerSecret."&code=";

				$this->_callMyProfile			= "https://graph.facebook.com/me";
				break;
				
			default:
				die("OAuthClient: Unknown service: '".$service."'");
		}	
		
		$this->serviceType = $service;
	}
	
	function setConsumerKey($key) {
		$this->consumerKey = $key;
		$this->setService($this->serviceType);
	}
	
	function setConsumerSecret($secret) {
		$this->consumerSecret = $secret;
		$this->setService($this->serviceType);
	}

	function setResponseUrl($url) {
		$this->responseUrl = $url;
		$this->setService($this->serviceType);
	}
	
    
    
    function connect() {
		if ($this->_oAuthVersion == 1) {
	
			try {	echo $this->consumerKey;
				$oauth = new OAuth($this->consumerKey, $this->consumerSecret);
			
				$request_token_info = $oauth->getRequestToken($this->_oAuthRequestTokenUrl, $this->responseUrl);
				
				if(!empty($request_token_info)) {
					$this->ci->session->set_userdata("oauth_token_secret", $request_token_info['oauth_token_secret']);
					header("Location: ".$this->_oAuthRedirectUrl.$request_token_info['oauth_token']);
				} else {
					print "Failed fetching request token, response was: " . $oauth->getLastResponse();
				}
			} catch(OAuthException $E) {
				//print_r($E);
				echo "hi bye"; exit;
				echo "Response (RequestToken): ". $E->lastResponse . "\n";
			}
		} else {
			// If oAuth v2, we must be sure the url's are correctly poppulated
			// therefor, set the service type again
			$this->setService($this->serviceType);
			header("Location: ".$this->_oAuthRedirectUrl);
		}
    }
    
    function response() {
		if ($this->_oAuthVersion == "1") {
			try {                        
				$oauth = new OAuth($this->consumerKey, $this->consumerSecret);
				$oauth->setToken($this->ci->input->get('oauth_token'), $this->ci->session->userdata("oauth_token_secret"));
				
				$access_token_info = $oauth->getAccessToken($this->_oAuthAccessTokenUrl);
				
				if(!empty($access_token_info)) {
					$this->oauthToken  		= $access_token_info['oauth_token'];
					$this->oauthTokenSecret = $access_token_info['oauth_token_secret'];
					return $this->userId;
				} else {
					print "Failed fetching access token, response was: " . $oauth->getLastResponse();
					return false;
				}
				
			} catch(OAuthException $E) {
				echo "Response: ". $E->lastResponse . "\n";
				return false;
			}
		} else {	
			if ($this->ci->input->get("code")) { 
			$ch=curl_init();
                       curl_setopt($ch, CURLOPT_URL, $this->_oAuthAccessTokenUrl.$this->ci->input->get("code"));
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      $response =curl_exec($ch);
                             curl_close($ch); 
							 print_r($response);die;
				//$response = file_get_contents($this->_oAuthAccessTokenUrl.$this->ci->input->get("code"));
			
				$accessToken = "";
				
				$matches = preg_split("/\&/",$response);
				foreach($matches as $match) {
					list($key, $value) = preg_split("/=/",$match);
					if ($key == "access_token") {
						 $this->oauthTokenSecret = $value;
					}
				}
                              
				// Fetch Remote user id
				$user = $this->_httpRequestJson($this->_callMyProfile, false);
				  
				return $user;
				
			} else {
				 echo '<script type="text/javascript">opener.location.href="'.base_url().'login"; window.close();</script>';
				//print "Error: ".$this->ci->input->get("error")." (".$this->ci->input->get("error_description").")";
			}
		}
    }


    function apiCall($call) {
		if ($this->_oAuthVersion == "1") {
			try {
				$oauth = new OAuth(
						$this->consumerKey,
						$this->consumerSecret,
						OAUTH_SIG_METHOD_HMACSHA1,OAUTH_AUTH_TYPE_URI); //initiate
								
				$oauth->setToken($this->consumerKey, $this->consumerSecret);
				$data = $oauth->fetch($call);
				$response_info = $oauth->getLastResponse();
				
				return new SimpleXMLElement($oauth->getLastResponse());
				
			} catch(OAuthException $E) {
				echo "Exception caught!\n";
				echo "Response: ". $E->lastResponse . "\n";
				return false;
			}
		} else {
			return $this->_httpRequestJson($call, true);
		}
    }
    
    function _httpRequestJson($url, $reauthenticateOnFailure = true, $requireAccessToken = true) {
		if ($requireAccessToken) {
			 $urldata = $url ."?access_token=". $this->oauthTokenSecret;
		}
		$ch=curl_init();
                       curl_setopt($ch, CURLOPT_URL, $urldata);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      $data =curl_exec($ch);
                             curl_close($ch); 
                        
		//$data = @file_get_contents($url);
		if (!$data) {
			if($reauthenticateOnFailure) {
				$this->connect();
			}
			return false;
		} else {
		
			return json_decode($data);
		}
	}
    
	function getProfile() {
		return $this->apiCall($this->_callMyProfile);
	}
}
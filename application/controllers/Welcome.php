<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/***
Name :Sasi Priya
Module : Online Book Shelf
Created :23-2-17
*/
class Welcome extends CI_Controller {
public function __construct() {
        parent::__construct();
       
		$this->load->model('login_model');
                
        
    }
	//google Login
	public function index()
	{
		
		if($this->session->userdata('login') == true){
			redirect('welcome/profile');
		}
		
		if (isset($_GET['code'])) {
			
			$this->googleplus->getAuthenticate();
			$this->session->set_userdata('login',true);
			$this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());
			redirect('welcome/profile');
			
		} 
			
		$contents['login_url'] = $this->googleplus->loginURL();
		
		 $this->view_page('welcome_message',$contents);
		
	}
	//Save Rating for user book
   public function saveFBprofile()
  {
    
 
		if($this->input->post('first_name'))
		$given_name = $this->input->post('first_name');
		if($this->input->post('last_name'))
		$family_name = $this->input->post('last_name');
	if($this->input->post('email'))
		$email = $this->input->post('email');
	if($this->input->post('profileimg'))
		echo $picture = $this->input->post('profileimg');
	if($this->input->post('log_type'))
		$log_type = $this->input->post('log_type');
	
	$this->session->set_userdata('login',true);
	$userdata=array('given_name'=>$given_name,'family_name'=>$family_name,'email'=>$email,'gender'=>'','picture'=>$picture,'log_type'=>'Facebook');
	$this->session->set_userdata('user_profile',$userdata);
		$contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		//echo $contents['user_profile']['picture'];
		  $emailExist=$this->login_model->count_rows('users',array('email'=>$email));
			   if($emailExist==0)
			   {
                         $data=array('first_name'=>$contents['user_profile']['given_name'],'last_name'=>$contents['user_profile']['family_name'],'email'=>$email,'gender'=>$contents['user_profile']['gender'],'picture_url'=>$contents['user_profile']['picture'],'log_type'=>'Facebook');
                                
                            $this->site_model->InsertData('users', $data);     
                             
                                
			   }else{
				    $data=array('first_name'=>$contents['user_profile']['given_name'],'last_name'=>$contents['user_profile']['family_name'],'email'=>$email,'gender'=>'','picture_url'=>$contents['user_profile']['picture'],'log_type'=>'Facebook');
                                
                          $cond=array("email"=>$email);	
							$this->site_model->UpdateData('users',$data,$cond);	   
			   }
			    $userid=$this->login_model->get_user_id($email);
			  $shelfdata= $this->login_model->fetchBookdata( $userid);	
			 // print_r( $shelfdata);
		$contents['myshelf'] = $shelfdata;
		$this->view_page('profile',$contents);//$this->load->view('profile',$contents);
                
   
  }
	//Home Page
	public function profile(){
		
		if($this->session->userdata('login') != true){
			redirect('');
		}
		
		$contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		
		  $emailExist=$this->login_model->count_rows('users',array('email'=>$email));
			   if($emailExist==0)
			   {
                         $data=array('first_name'=>$contents['user_profile']['given_name'],'last_name'=>$contents['user_profile']['family_name'],'email'=>$email,'gender'=>$contents['user_profile']['gender'],'picture_url'=>$contents['user_profile']['picture'],'log_type'=>'Google');
                                
                            $this->site_model->InsertData('users', $data);     
                             
                                
			   }else{
				    $data=array('first_name'=>$contents['user_profile']['given_name'],'last_name'=>$contents['user_profile']['family_name'],'email'=>$email,'gender'=>$contents['user_profile']['gender'],'picture_url'=>$contents['user_profile']['picture'],'log_type'=>'Google');
                                
                          $cond=array("email"=>$email);	
							$this->site_model->UpdateData('users',$data,$cond);	   
			   }
			    $userid=$this->login_model->get_user_id($email);
			  $shelfdata= $this->login_model->fetchBookdata( $userid);	
			 // print_r( $shelfdata);
		$contents['myshelf'] = $shelfdata;
		$this->view_page('profile',$contents);//$this->load->view('profile',$contents);
		
	}
	//List book from bookname
	function listbooks()
	{
	
		$bookname= "";
		
		if($this->input->post('bookname'))
		$bookname = $this->input->post('bookname');
		$contents['myshelf'] =  $this->login_model->fetchBooklist($bookname);
		$this->load->view("profile_ajax",$contents);
	}
	//Add book
	public function addbook()
	{
		
		$bookname= "";
		$this->load->view("addbook");
	}
	//Savebook
	public function savebook()
  {
	  
     $fields_data=$this->input->post();
     //print_r($fields_data);
		$contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		$userid=$this->login_model->get_user_id($email);
		$tbl="books";
                 
		if(isset($fields_data['submit']))
		{
			
		    $check_duplicate=array("bookTitle"=>$fields_data['bookTitle'],"bookAuthour"=>$fields_data['bookAuthor']);
					
			if($this->login_model->FetchBookCount($tbl,$sel='*',$check_duplicate)>0)
			{
                            $this->session->set_flashdata('succ_message','Book is Already Exist..!!!');
                            redirect(base_url().'index.php/welcome');
				//$data['error'] = "Record is Already Exist..!!!";
			}				
			else
			{	
                                if(!empty($_FILES['bookImage']['name'])){
									 $image = strtotime(date("Y-m-d H:i:s"))."_".$_FILES['bookImage']['name'];
									 move_uploaded_file($_FILES['bookImage']["tmp_name"],"./assets/uploads/".$image);
									 
								 }else{
									 $image  ='';
					$isbn = "0";			 }
			  $isbn = "ISBN".time();//rand(10,13);
						$data=array("bookTitle"=>$fields_data['bookTitle'],"bookAuthour"=>$fields_data['bookAuthor'],"bookDesc"=>$fields_data['bookDesc'],"bookImage"=>$image,"bookIsbn"=>$isbn,"createdDate"=>date('Y-m-d H:i:s'));
                                  $insert_id= $this->site_model->InsertData($tbl,$data);
                                    if(!empty($insert_id)){
                                        $userdata = array('userId'=>$userid,'bookId'=>$insert_id,'createdDate'=>date('Y-m-d H:i:s'));
										  $insert_id= $this->site_model->InsertData('userbookshelf',$userdata);
                                    }
                                    // $data['smsg'] = 'Register Successfully, Please activate Your account. ';
                                    $this->session->set_flashdata('succ_message','Your data successfully Added.');
                                    redirect(base_url().'index.php/welcome');
                                
			}	
		}

	
  }
  //Show edit bookdata
   public function editbook($id="")
  {
    
    $tbl="books";
     
    $fields_data=$this->input->post();
      if(isset($fields_data['submit']))
      {
          $check_duplicate=array("bookTitle"=>$fields_data['bookTitle'],"bookAuthour"=>$fields_data['bookAuthor']);
            if(count($this->login_model->FetchBookeditCount($tbl,$sel='*',$check_duplicate,$id))>0)
            {
               
                                       $this->session->set_flashdata('succ_message','Book is Already Exist..!!!');
                            redirect(base_url().'index.php/welcome');
            }else{
               
				if(!empty($_FILES['bookImage']['name'])){
									 $image = strtotime(date("Y-m-d H:i:s"))."_".$_FILES['bookImage']['name'];
									 move_uploaded_file($_FILES['bookImage']["tmp_name"],"./assets/uploads/".$image);
									 
								 }else{
									 $image  =$fields_data['bookcover'];
								 }
					
						$updatedata=array("bookTitle"=>$fields_data['bookTitle'],"bookAuthour"=>$fields_data['bookAuthor'],"bookDesc"=>$fields_data['bookDesc'],"bookImage"=>$image,"createdDate"=>date('Y-m-d H:i:s'));
						//print_r($updatedata);die;
                $this->site_model->UpdateData('books',$updatedata,array('bookId'=>$fields_data['hdnedit']));
                $this->session->set_flashdata('succ_message','Your data successfully updated.');
                redirect(base_url()."index.php/welcome");
            }
          
      }else{
          $data['fetchedit']=$this->login_model->fetchrecord($id);
         // print_r($data['fetchedit']);
           $this->view_page('editbook',$data);
           
      }
   
  }
  //Save Shelf 
  public function saveshelf($bookid="")
  {
    
    $tbl="userbookshelf";
	$contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		 $userid=$this->login_model->get_user_id($email);//echo $bookid;
     $userdata = array('userId'=>$userid,'bookId'=>$bookid,'createdDate'=>date('Y-m-d H:i:s'));
	$check_duplicate = array('userId'=>$userid,'bookId'=>$bookid);
	//echo  count($this->login_model->FetchBookCount($tbl,$sel='*',$check_duplicate));die;
	  if(count($this->login_model->FetchBookCount($tbl,$sel='*',$check_duplicate))>0)
            {
               
                                       $this->session->set_flashdata('succ_message','Book is Already Exist..!!!');
                            redirect(base_url().'index.php/welcome');
            }else{
				  $insert_id= $this->site_model->InsertData('userbookshelf',$userdata);
                $this->session->set_flashdata('succ_message','Your data successfully updated.');
                redirect(base_url()."index.php/welcome");
			}
										 
    
                
   
  }
  //Save Rating for user book
   public function saverating()
  {
    
    $tbl="userbookshelf";
	$contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		$userid=$this->login_model->get_user_id($email);
		if($this->input->post('rating'))
		$rating = $this->input->post('rating');
		if($this->input->post('bookid'))
		$bookid = $this->input->post('bookid');
     $userdata = array('rating'=>$rating);
	 $cond = array('shelfId'=>$bookid);
	$insert_id= $this->site_model->UpdateData('userbookshelf',$userdata,$cond);
               
     $shelfdata= $this->login_model->fetchBookdata( $userid);	
			  //print_r( $shelfdata);
		$contents['myshelf'] = $shelfdata;
		$this->load->view('profile_list',$contents);
		
                
   
  }
  //List User
   public function listusers()
  {
    
    $contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		$userid=$this->login_model->get_user_id($email);
		$username="";
		if($this->input->post('username'))
			$username = $this->input->post('username');
			$contents['myshelf'] =  $this->login_model->fetchuserlist($username);
			$this->load->view("profile_user",$contents);
	 }
	 //List other user books
	 public function listotheruserbooks($userid="")
	{
		
		 $contents['user_profile'] = $this->session->userdata('user_profile');
		$email= $contents['user_profile']['email'];
		$useridlog=$this->login_model->get_user_id($email);
		
		$contents['myshelf'] =  $this->login_model->fetchBookdata($userid);
		$contents['userid']=$useridlog;
		$this->load->view("profile_others",$contents);
	}
	//View book
	 public function viewbook($id="")
  {
    
         $data['fetchedit']=$this->login_model->fetchrecord($id);
         // print_r($data['fetchedit']);
		  $data['user_profile'] = $this->session->userdata('user_profile');
               $this->view_page('profile_details',$data);
      
   
  }
  public function logout(){
		
		$this->session->sess_destroy();
		$this->googleplus->revokeToken();
		redirect('');
		
	}
	public function view_page($page,$data=array())
	{
			  
	    $this->load->view('header', $data);
    	$this->load->view($page, $data);
	}
}

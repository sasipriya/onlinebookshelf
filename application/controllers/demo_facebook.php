<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Demo_facebook extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'cookie'));
        $this->load->library(array('oauthclient'));
        $this->load->model('login_model');
        $this->oauthclient->setService("facebook");
        $this->oauthclient->setConsumerKey("594145600793475");
        $this->oauthclient->setConsumerSecret("f3aa725191432c08f36609f21d51206f");
        $this->oauthclient->setResponseUrl(base_url() . "index.php/demo_facebook/response");
        require_once(APPPATH . 'controllers/welcome.php');
    }

    function index() {
        if (!empty($_GET['redirect_url'])) {
            $this->session->set_userdata('redirect_url', $_GET['redirect_url']);
            //$redirect_url  = $firstLoginData['redirect_url'];
        }
        $this->oauthclient->connect();
    }

    function response() {
        
        $user = $this->oauthclient->response();
       
        print_r($user);die;
        if (isset($socialCookie) && (!empty($socialCookie))) {
            $first_name = $user->first_name;
            $last_name = $user->last_name;
            $gender = $user->gender;
            $password = genRandomString(9);
            $email = $user->email;
            $nameArray = explode("@", $email);
            $username = $nameArray[0];
            $user_type = $socialCookie;
            $emailExist = $this->login_model->count_rows('login', array('email' => $email));
            if ($emailExist == 0) {
                $data = array('soc_username' => $username, 'soc_password' => $password, 'soc_email' => $email, 'soc_firstname' => $first_name, 'soc_lastname' => $last_name, 'soc_college' => $college, 'soc_company' => $company, 'soc_location' => $location, 'soc_gender' => $gender);
                $this->session->set_userdata($data);
                $userData = array('username' => $username, 'password' => $password, 'email' => $email, 'user_type' => $user_type, 'pic_id' => $user->id, 'soc_type' => 'facebook');
                delete_cookie("social_usertype");
                login::setSocialSession($userData);
            } else {
                $user_id = $this->login_model->get_user_id($email);
                delete_cookie("social_usertype");
                $userData = $this->login_model->FetchData('login', 'id,username,password,email,user_type,status', array('id' => $user_id));
                if (!empty($userData)) {
                    foreach ($userData as $user) {
                        $data['error_msg'] = "Dear " . $user['username'] . ", your Email ID: '" . $email . "' is already registered with us";
                        $this->load->view('loginview/user_message', $data);
                    }
                }
            }
        } else {

            $first_name = $user->first_name;
            $last_name = $user->last_name;
            $gender = $user->gender;
            $password = genRandomString(9);
            $email = $user->email;
            $nameArray = explode("@", $email);
            $username = $nameArray[0];
            $data = array('soc_username' => $username, 'soc_password' => $password, 'soc_email' => $email, 'pic_id' => $user->id, 'soc_type' => 'facebook', 'soc_firstname' => $first_name, 'soc_lastname' => $last_name, 'soc_college' => $college, 'soc_company' => $company, 'soc_location' => $location, 'soc_gender' => $gender);
            $this->native_session->set_userdata($data);

            $emailExist = $this->login_model->count_rows('login', array('email' => $email));
            if ($emailExist == 0) {
                $this->load->view('loginview/registration_after');
            } else {
                $user_id = $this->login_model->get_user_id($email);
                $userData = $this->login_model->FetchData('login', 'id,username,password,email,user_type,status', array('id' => $user_id));
                if (!empty($userData)) {
                    foreach ($userData as $user) {
                        login::setSocialSession($user);
                    }
                }
            }
        }
    }

}

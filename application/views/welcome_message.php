<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Online bookshelf</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Online Book Shelf</h1>

	<div id="body">
		<p style="text-align:center;"><a href="<?php echo $login_url;?>"><img src="./assets/sign_in.jpg" width="300"></a></p>
		<p style="text-align:center;font-weight:bold;font-size:14px;">OR </p>
		<p style="text-align:center;color:blue;"><button type="submit" class="btn flex regular_14 azh-but-color-fb azh-margin-bottom-25" onclick="facebooklogin();"><i class="fa fa-facebook new-icon"></i>&nbsp;&nbsp;Sign in with Facebook</button></p>
	</div>

		  
	
	
</div>

</body>
 

<div id="fb-root"></div>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '594145600793475', // App ID     
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
    
    
	FB.Event.subscribe('auth.authResponseChange', function(response) 
	{
 	 if (response.status === 'connected') 
  	{
  		document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
  		//SUCCESS
  		
  	}	 
	else if (response.status === 'not_authorized') 
    {
    	document.getElementById("message").innerHTML +=  "<br>Failed to Connect";

		//FAILED
    } else 
    {
    	document.getElementById("message").innerHTML +=  "<br>Logged Out";

    	//UNKNOWN ERROR
    }
	});	
	
    };
    
   	function facebooklogin()
	{
	
		FB.login(function(response) {
		   if (response.authResponse) 
		   {
		    	getUserInfo();
  			} else 
  			{
  	    	 console.log('User cancelled login or did not fully authorize.');
   			}
		 },{scope: 'email,user_photos,user_videos,user_friends'});
	
	
	}

  function getUserInfo() {
  
   /*FB.api('/me/invitable_friends', function(response) {
  
                    console.log(response)
                });*/
			/*FB.api('/me/invitable_friends', {fields: 'name,id,location,birthday'}, function(response) {
  console.log(response)
});	*/

		/*	FB.api('/me/invitable_friends',{fields: 'name,id,location,birthday'}, function(response) {
			console.log('Got friends: ',  response.data.length);

			var friends = response.data;

			for (var i=0; i < friends.length && i < 25; i++) {
			var friend = friends[i];
			   console.log(friend.id);
			}

			},{scope: 'user_friends'});	*/
			
	    FB.api('/me?fields=first_name, last_name, picture, email', function(response) {
	console.log(response);
			var user_fname = '';
			if(response.first_name!=''){
			  user_fname = response.first_name;
			}
			var user_lname = '';
			if( response.last_name!=''){
			  user_lname = response.last_name;
			}
			var imageurl = 'https://graph.facebook.com/'+response.id+'/picture?type=large';			
			var parameter = 'first_name='+user_fname+'&last_name='+user_lname+'&user_email='+response.email+'&user_socialid='+response.id+'&user_logintype=3'+'&user_profileimage='+imageurl;
			//alert(parameter);
			$.ajax({
		 type:'POST',
		  url:'<?php echo base_url(); ?>index.php/welcome/saveFBprofile/',
		  data:{'first_name':user_fname,'last_name':user_lname,'email':response.email,'log_type':'Facebook','profileimg':imageurl},
		  success:function(data) {
			//  alert(data);
		  		window.location.href="<?php echo base_url(); ?>index.php/welcome/profile";
					
				}
				 
		  
	}); 	
			
    });
	
	
		
	/*FB.getLoginStatus(function(response) {
		
		  console.log(response.authResponse.accessToken);
	 });	*/	
	
    }
	
 
	/*
	function getPhoto()
	{
	  FB.api('/me/picture?type=normal', function(response) {

		  var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
	  	  document.getElementById("status").innerHTML+=str;
	  	  	    
    });
	
	}
	*/
	function facebooklogout()
	{
		FB.logout(function(){document.location.reload();});
	}

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));

</script>

</html>
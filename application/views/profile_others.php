<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Online Bookshelf</title>
<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/star-rating.css" media="all" type="text/css"/>
    <link rel="stylesheet" href="<?=base_url()?>assets/css/themes/krajee-fa/theme.css" media="all" type="text/css"/>
    <link rel="stylesheet" href="<?=base_url()?>assets/css/themes/krajee-svg/theme.css" media="all" type="text/css"/>
    <link rel="stylesheet" href="<?=base_url()?>assets/css/themes/krajee-uni/theme.css" media="all" type="text/css"/>
   
    <script src="<?=base_url()?>assets/js/star-rating.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/js/themes/krajee-fa/theme.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/js/themes/krajee-svg/theme.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/js/themes/krajee-uni/theme.js" type="text/javascript"></script>
	<style type="text/css">
	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }
	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}
	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}
	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}
	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}
	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Online Bookshelf</h1>

	<div id="body">
		<h1>welcome <?php echo @$user_profile['name'];?> &nbsp;<img src="<?php echo $user_profile['picture'];?>" width="25"> &nbsp;<a href="<?php echo site_url('welcome/logout');?>">Sign Out</a><h1>
		 <a href="<?php echo base_url();?>index.php/welcome/addbook">Add Book</a>
	 <div>
	 <input type="text" name="txtsearch" id="txtsearch" value="" placeholder="Search by Title or ISBN">
	  <input type="text" name="txtuser" id="txtuser" value="" placeholder="Search by username">
	  		 <ul id="ajax_list_wrapper">
            <?php
              
              //echo $currenttime;echo"next"; 
              $count = count($myshelf);
			  if($count>0){
              foreach ($myshelf as $key=>$shelf) {
                   
            ?>
                  <li><a href="<?php echo base_url();?>index.php/welcome/viewbook/<?=$shelf->bookId?>"><p><img src="<?=base_url()?>assets/uploads/<?php echo $shelf->bookImage?>" width="50" height="50"></p>Title:<?php echo $shelf->bookTitle?></a></li>
				  <li>ISBN:<?php echo $shelf->bookIsbn ?></li>
				  <li>Authour:<?php echo $shelf->bookAuthour ?></li>
				 <?php if($shelf->rating!=""){
					 $rating = $shelf->rating;
				 }else{
					  $rating =0;
				 }?>
				 
				   <?php if($userid==$shelf->userId){?><a href="<?php echo base_url();?>index.php/welcome/editbook/<?=$shelf->bookId?>">edit</a>
				         <input type="text" class="rating rating-loading" value="<?=$rating?>" data-size="xs" id="rating<?=$key?>" title="" onchange="saverate('<?=$shelf->shelfId?>','<?=$key?>');">
						 <?php }else{?>Rating :<?=$rating?> stars<?php }?>
            <?php
              }}else{ echo "No Books found in your shelf";}
            ?>
            <p><a href="<?php echo base_url();?>">Back To Myself</a></p>
            </ul>
	 </div>
	</div>

	
</div>

</body>
<script>
$("#txtsearch").change(function(){

		var locSelected = $("#txtsearch").val();
		//jQuery('#ajax_loader').show();
		$.ajax({
		 type:'POST',
		  url:'<?php echo base_url(); ?>index.php/welcome/listbooks/',
		  data:{'bookname':locSelected},
		  success:function(data) {
		  		
				 $("#ajax_list_wrapper").html(data);
				// jQuery('#ajax_loader').hide();
		  		/* if(total_cont<=12)
				 $("#loadmore").hide();
				 else
				 $("#loadmore").show();*/
		  }
	}); 	
	});
	$("#txtuser").change(function(){

		var username = $("#txtuser").val();
		//jQuery('#ajax_loader').show();
		$.ajax({
		 type:'POST',
		  url:'<?php echo base_url(); ?>index.php/welcome/listusers/',
		  data:{'username':username},
		  success:function(data) {
		  		
				 $("#ajax_list_wrapper").html(data);
				// jQuery('#ajax_loader').hide();
		  		/* if(total_cont<=12)
				 $("#loadmore").hide();
				 else
				 $("#loadmore").show();*/
		  }
	}); 	
	});
	function saverate(bookid,key){
	var rating=$('#rating'+key).val();
					//alert(rating);
					//alert(bookid);
					$.ajax({
		 type:'POST',
		  url:'<?php echo base_url(); ?>index.php/welcome/saverating/',
		  data:{'bookid':bookid,'rating':rating},
		  success:function(data) {
		  		
				 $("#ajax_list_wrapper").html(data);
				// jQuery('#ajax_loader').hide();
		  		/* if(total_cont<=12)
				 $("#loadmore").hide();
				 else
				 $("#loadmore").show();*/
		  }
	}); 	
}
</script>
</html>
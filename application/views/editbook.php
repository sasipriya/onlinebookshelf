 <form action="<?=base_url()?>index.php/welcome/editbook" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <!--  <form data-parsley-validate="" id="form2" role="form" name="form2" novalidate>-->
            
        <div class="row">
          <div class="form-group col-md-4">
        <label for="contactemail">Book Title: </label>
        <input type="text" class="form-control" id="field0" placeholder="Book Title" name="bookTitle" data-parsley-id="error0" maxlength="100" value="<?php echo $fetchedit->bookTitle?>">
        <ul class="parsley-errors-list" style="color:red; font-size:10px;" id="error0">
        </ul>
      </div>
              <div class="form-group col-md-4">
      <label for="contactemail">Author: </label>
      <input type="text" class="form-control" id="field1" placeholder="Book Author" name="bookAuthor" data-parsley-id="error1" maxlength="50" value="<?php echo $fetchedit->bookAuthour?>">
        <ul class="parsley-errors-list" style="color:red; font-size:10px;" id="error1">
        </ul>
      </div>
      <div class="form-group col-md-4">
        <label for="contactemail">Book cover: </label>
        <input type="file" class="form-control" id="field2" placeholder="bookcover" name="bookImage" data-parsley-id="error2">
		<p><img src="<?=base_url()?>assets/uploads/<?php echo $fetchedit->bookImage?>" width="50" height="50"></p>
		<input type="hidden"name="bookcover" value="<?php echo $fetchedit->bookImage?>">
        <ul class="parsley-errors-list" style="color:red; font-size:10px;" id="error2">
        </ul>
      </div>
      <div class="form-group col-md-4">
        <label for="contactemail">Description: </label>
        <textarea class="form-control" id="field3" placeholder="Book Description"  name="bookDesc" data-parsley-id="error3" row="4" col="5"><?php echo $fetchedit->bookDesc?></textarea>
        <ul class="parsley-errors-list" style="color:red; font-size:10px;" id="error3">
        </ul>
		  <input type="hidden" name="hdnedit" value="<?php echo $fetchedit->bookId?>">
      </div>
     
    </div>
   
  </div>
  <div class="modal-footer">
      <input type="submit" name="submit" class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" style="text-align:center; padding-left: 30px; padding-right: 30px;" value="Submit" onclick="return validateBook1();">
    <a href="<?=base_url()?>"> Cancel</a>
  </div>
  
		 
		 </html>